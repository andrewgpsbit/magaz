jQuery(function($){


    $('form#import').submit(function(e){
        //e.preventDefault()
        data = $(this).serialize();
        console.log(data);
    });

    $(document).on('click', '.insert', function(e){
        e.preventDefault()
        $.oc.stripeLoadIndicator.show();
        $(this).hide();
        $.request('onSaveImport', {
            complete: function (data) {
                $.oc.stripeLoadIndicator.hide();
                this.complete(data);
            },
            success: function(data) {
                //console.log('success', data.categories);
                this.success(data);
            }
        });
    });

});