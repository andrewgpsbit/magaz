jQuery(function($){

    //var treeData = [
    //    { "id": "1", "parent": "#", "text": "Category 1"},
    //    { "id": "2", "parent": "1", "text": "Category 2"},
    //    { "id": "3", "parent": "1", "text": "Category 3" },
    //    { "id": "4", "parent": "#", "text": "Category 4" },
    //    { "id": "5", "parent": "4", "text": "Category 5"},
    //    { "id": "6", "parent": "4", "text": "Category 6" }
    //];

    function initTree(data) {
        //console.log('initTree', data)
        //console.log('initTree treeData', treeData) changed.jstree
        $('#tree').on('move_node.jstree', function (e, data) {
            var weight = [];
            $('#tree li').each(function(index, b){
                var id = $(this).attr('id');
                weight[id] = index+1;
            });
            changeDepth({id: data.node.id, parent: data.node.parent, weight: weight})
        }).jstree({
            core: {
                data: data,
                check_callback: true
            },
            plugins: ['dnd', 'conditionalselect']
        });

        $('#tree').on('changed.jstree', function (e, data) {
            console.log('changed', data)
            window.location.href = data.node.original.href;
        });
    }

    function changeDepth(data){
        $.oc.stripeLoadIndicator.show();
        $.request('onSetPosition', {
            data: data,
            //update: {
            //    'calendar::calendar': '.js-calendar'
            //},
            complete: function (data) {
                $.oc.stripeLoadIndicator.hide();
                this.complete(data);
            },
            success: function(data) {


                this.success(data);
            }
        });
    }

    $.request('onGetAll', {
        complete: function (data) {
            $.oc.stripeLoadIndicator.hide();
            this.complete(data);
        },
        success: function(data) {
            //console.log('success', data.categories);
            initTree(data.categories);

            this.success(data);
        }
    });


});