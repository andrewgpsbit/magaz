<?php namespace Mag\App\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Brand Back-end Controller
 */
class Brand extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Mag.App', 'app', 'brand');
    }
}