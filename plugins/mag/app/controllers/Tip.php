<?php namespace Mag\App\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Request;
use Input;
use Storage;
use App;
use Maatwebsite\Excel\Excel;
use PHPExcel_Cell;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Worksheet_MemoryDrawing;
use Mag\App\Models\Product;


/**
 * Tip Back-end Controller
 */
class Tip extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Mag.App', 'app', 'tip');
        $this->render();
    }

    const LIMIT = 1000;

    public function render(){
        $this->addCss('/plugins/mag/app/assets/css/backend.css');

        $this->addJs('/plugins/mag/app/assets/js/backend/import.js');

        if(!empty(Input::file())){
            Storage::put(
              'import.xls',
              file_get_contents(Request::file('file')->getRealPath())
            );
            sleep(0.1);
            $this->saveImportImages();
        }

        //return $this->massDelete();

        //$this->saveImport();
    }

    public function massDelete() {
        for($i=90; $i< 500; $i++){
            $product = Product::find($i);
            if($product){
                $product->delete();
                //sleep(0.1);
            }
        }
    }

    public function showImport() {

        if(!Storage::exists('import.xls')){
            return;
        }

        $products = [];
        $sProducts = '<a href="/backend/mag/app/tip" class="insert btn btn-primary oc-icon-plus">Добавить продукты на сайт</a>';
        $sProducts .= '<table class="import"><tr><th>Категория</th><th>Название</th><th>Фото</th><th>Розница</th><th>Опт</th></tr>';
        /** @var Excel $excel */
        $objReader = App::make('excel');
        $objReader->load(storage_path('app/') .'import.xls', function($reader) use (&$products, &$sProducts) {
            $render =  $reader->limit(self::LIMIT)->get();
            foreach($render as $key => $sheet){
                $nId = '';
                foreach($sheet as $key2 => $sheet2){
                    $id = trim($sheet2->id);
                    if(strpos($id,'_cat') !== false){
                        $aId = explode(' ',$id);
                        $nId = intval(array_shift($aId));
                    }
                    if(!is_int($nId)){continue;}
                    if(empty($sheet2->name)){continue;}

                    $category_id = $nId;
                    $name = (!empty($sheet2->name))?$sheet2->name:'';
                    $price = (!empty($sheet2->cost))?$sheet2->cost:'';
                    $discount = (!empty($sheet2->costopt))?$sheet2->costopt:'';
                    $desc = (!empty($sheet2->desc))?$sheet2->desc:'';
                    $photos = '';
                    if(Storage::exists('images/E'.($key2+2).'.jpg')){
                        $photos = '/storage/app/images/E'.($key2+2).'.jpg';
                    }
                    $products[] = compact('category_id', 'name', 'price', 'discount', 'desc', 'photos');

                    $sProducts .= '<tr>';
                    $sProducts .= sprintf('<td>%s</td>', $category_id);
                    $sProducts .= sprintf('<td>%s</td>', $name);
                    $sProducts .= sprintf('<td><img src="%s" width="25" height="25" /></td>', (!empty($photos))? $photos:'/themes/corlate/assets/images/no_photo_small.png');
                    $sProducts .= sprintf('<td>%s</td>', $price);
                    $sProducts .= sprintf('<td>%s</td>', $discount);
                    //$sProducts .= sprintf('<td>%s</td>', $desc);
                    $sProducts .= '</tr>';
                }
            }
        });
        return $sProducts .= '</table>';
    }

    public function onSaveImport() {
        $this->saveImport();
    }

    public function saveImport() {

        /** @var Excel $excel */
        $objReader = App::make('excel');
        $objReader->load(storage_path('app/') .'import.xls', function($reader) {
            //$reader->limit(10)->dd();
            $render =  $reader->limit(self::LIMIT)->get();
            foreach($render as $key => $sheet){

                $nId = '';

                foreach($sheet as $key2 => $sheet2){

                    $id = trim($sheet2->id);

                    if(strpos($id,'_cat') !== false){
                        $aId = explode(' ',$id);
                        $nId = intval(array_shift($aId));
                    }

                    if(!is_int($nId)){
                        continue;
                    }

                    //if($key2 == 3553){

                    $product = new Product();

                    if(empty($sheet2->name)){
                        continue;
                    }
                    $product->category_id = $nId;
                    $product->name = (!empty($sheet2->name))?$sheet2->name:'';
                    $product->price = (!empty($sheet2->cost))?$sheet2->cost:'';
                    $product->discount = (!empty($sheet2->costopt))?$sheet2->costopt:'';
                    $product->desc = (!empty($sheet2->desc))?$sheet2->desc:'';
                    $product->save();

                    if(Storage::exists('images/E'.($key2+2).'.jpg')){
                        $product->photos()->create(['data'=>storage_path('app/').'images/E'.($key2+2).'.jpg']);
                        $product->save();
                    }
                }
            }

        });
    }

    public function saveImportImages() {

        Storage::deleteDirectory('images');
        sleep(0.2);
        Storage::makeDirectory('images');

        $objReader = App::make('excel');

        $xlsFile = storage_path('app/') .'import.xls';
        $data = $objReader->load($xlsFile);
        $objWorksheet = $data->getActiveSheet();
        foreach ($objWorksheet->getDrawingCollection() as $drawing) {
            //for XLSX format
            $string = $drawing->getCoordinates();
            $coordinate = PHPExcel_Cell::coordinateFromString($string);
            if ($drawing instanceof PHPExcel_Worksheet_MemoryDrawing) {
                $image = $drawing->getImageResource();
                // save image to disk
                $renderingFunction = $drawing->getRenderingFunction();
                switch ($renderingFunction) {
                    case PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG:
                        //dd($string);
                        imagejpeg($image, storage_path('app/').'images/' . $string.'.jpg');
                        //dd(imagejpeg($image, storage_path('app/').'images/' . $drawing->getIndexedFilename()));
                        break;
                    case PHPExcel_Worksheet_MemoryDrawing::RENDERING_GIF:
                        //imagegif($image, 'uploads/' . $drawing->getIndexedFilename());
                        imagegif($image, storage_path('app/').'images/' . $string.'.gif');
                        break;
                    case PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG:
                    case PHPExcel_Worksheet_MemoryDrawing::RENDERING_DEFAULT:
                        //imagepng($image, 'uploads/' . $drawing->getIndexedFilename());
                        imagepng($image, storage_path('app/').'images/' . $string.'.jpg');
                        break;
                }
            }
        }
    }
}
