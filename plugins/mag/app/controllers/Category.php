<?php namespace Mag\App\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Mag\App\Models\Category as MCategory;

/**
 * Category Back-end Controller
 */
class Category extends Controller
{
    public $implement = [
      'Backend.Behaviors.FormController',
      'Backend.Behaviors.ListController',
      'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Mag.App', 'app', 'category');
        $this->render();
    }

    public function render(){
        $this->addCss('/plugins/mag/app/assets/css/backend.css');
        $this->addCss('/plugins/mag/app/assets/bower_components/jstree/dist/themes/default/style.css');

        $this->addJs('/plugins/mag/app/assets/bower_components/jstree/dist/jstree.min.js');
        $this->addJs('/plugins/mag/app/assets/js/category/script.js');
    }

    public function onGetAll(){

        $categories = MCategory::orderBy('weight')->get()->transform(function ($item, $key) {
            $parent = ($item->parent_id) ? $item->parent_id : '#' ;
            return [ "id" => $item->id, "parent" => $parent, "text" => $item->name, "state" => ["opened" => true],
              "href"=>'/backend/mag/app/category/update/'.$item->id ];
        })->toArray();
        $return = [
          "id"=>1,
          "text"=>"Root node",
          "state" => ["opened" => true ],
          "children" => $categories
        ];

        return ['categories' => $categories];
    }

    public function onSetPosition(){
        $id = str_replace('term', '', post('id'));
        $category = MCategory::find($id);
        $category->parent_id = intval(post('parent'));
        $category->save();
        foreach(post('weight') as $key => $weight){
            $category = MCategory::find($key);
            if($category){
                $category->weight = $weight;
                $category->save();
            }
        }
        $this::setDepth();
    }

    public static function setDepth(){

        $nullVariant = ['#', null, 0, '0', ''];

        $result = MCategory::orderBy('weight')->get()->keyBy('id')->toArray();

        foreach($result as $key => $term){
            $depth = 0;
            $parent = intval($term['parent_id']);
            if( in_array($parent, $nullVariant) ) {
                $result[$key]['depth'] = 0; //dd($term);
            }else{
                $result[$key]['depth'] = ++$depth;
                $exit = 0;
                while (true) {
                    if( in_array(trim($result[$parent]['parent_id']), $nullVariant)  ){
                        break;
                    }else{
                        $result[$key]['depth'] = ++$depth;
                        $parent = intval($result[$parent]['parent_id']);
                    }

                    $exit++;
                    if($exit>200){ break;}
                }
            }
        }

        foreach($result as $key => $item){
            $category = MCategory::find($key);
            if($category){
                $category->depth = $item['depth'];
                $category->save();
            }
        };
        //dd($result);
    }
}