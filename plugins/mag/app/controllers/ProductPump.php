<?php namespace Mag\App\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Product Pump Back-end Controller
 */
class ProductPump extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Mag.App', 'app', 'productpump');
    }
}
