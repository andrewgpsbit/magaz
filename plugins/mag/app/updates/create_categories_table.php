<?php namespace Mag\App\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('mag_app_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('parent_id');
            $table->string('url');
            $table->integer('weight');
            $table->integer('depth');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mag_app_categories');
    }


}
