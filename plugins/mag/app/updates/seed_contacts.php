<?php namespace Mag\App\Updates;

use Seeder;
use Db;
use Mag\App\Controllers\Category;

class SeedContactsTable extends Seeder
{
  public function run()
  {
    Db::table('mag_app_contacts')->insert([
      [ 'phone' => '+7 (928) 900 60 05','email' => 'tip007-86@mail.ru']
    ]);
  }
}