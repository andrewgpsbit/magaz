<?php namespace Mag\App\Updates;

use Seeder;
use Db;
use Mag\App\Controllers\Category;

class SeedUsersTable extends Seeder
{
  public function run()
  {
    Db::table('mag_app_users')->insert([
      [ 'phone' => '79289006005'],
      [ 'phone' => '79289006666'],
    ]);
  }
}