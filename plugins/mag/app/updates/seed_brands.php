<?php namespace Mag\App\Updates;

use Seeder;
use Db;
use Mag\App\Controllers\Category;

class SeedBrandsTable extends Seeder
{
  public function run()
  {
    Db::table('mag_app_brands')->insert([
      [ 'name' => 'Ariston'],
      [ 'name' => 'Zannussi'],
      [ 'name' => 'Basf'],
      [ 'name' => 'Pasabahche'],
    ]);
  }
}