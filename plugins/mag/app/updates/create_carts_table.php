<?php namespace Mag\App\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCartsTable extends Migration
{
    public function up()
    {
        Schema::create('mag_app_carts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();

            $indexes = [
              'user_id',
            ];

            foreach ($indexes as $index) {
                $table->index($index);
            }

        });


    }

    public function down()
    {
        Schema::dropIfExists('mag_app_carts');
    }
}
