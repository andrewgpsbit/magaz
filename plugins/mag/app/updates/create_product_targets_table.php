<?php namespace Mag\App\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductTargetsTable extends Migration
{
    public function up()
    {
        Schema::create('mag_app_product_targets', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('imageable_type');
            $table->integer('imageable_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mag_app_product_targets');
    }
}
