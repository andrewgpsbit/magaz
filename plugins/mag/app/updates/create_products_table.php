<?php namespace Mag\App\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('mag_app_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('sku')->nullable();
            $table->integer('price')->nullable();
            $table->integer('discount')->nullable();
            $table->boolean('promo')->nullable();
            $table->integer('category_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->text('desc')->nullable();
            $table->timestamps();
            //$table->foreign('category_id')->references('id')->on('categories');
        });

    }

    public function down()
    {
        Schema::dropIfExists('mag_app_products');
    }
}
