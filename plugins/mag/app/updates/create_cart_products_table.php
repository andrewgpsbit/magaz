<?php namespace Mag\App\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCartProductsTable extends Migration
{
    public function up()
    {
        Schema::create('mag_app_cart_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('cart_id')->unsigned();
            $table->integer('product_target_id')->unsigned();
            $table->integer('count')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cart_id')->references('id')->on('mag_app_carts');
            $table->foreign('product_target_id')->references('id')->on('mag_app_product_targets');
            $table->unique(['cart_id', 'product_target_id']);

            $indexes = ['cart_id','product_target_id'];

            foreach ($indexes as $index) {
                $table->index($index);
            }
        });

    }

    public function down()
    {
        Schema::dropIfExists('mag_app_cart_products');
    }
}
