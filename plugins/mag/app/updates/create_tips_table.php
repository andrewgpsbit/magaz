<?php namespace Mag\App\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTipsTable extends Migration
{
    public function up()
    {
        Schema::create('mag_app_tips', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('title');
            $table->integer('weight');
            $table->text('text');
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mag_app_tips');
    }
}
