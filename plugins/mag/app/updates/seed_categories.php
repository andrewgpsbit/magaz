<?php namespace Mag\App\Updates;

use Seeder;
use Db;
use Mag\App\Controllers\Category;

class SeedCategoriesTable extends Seeder
{
  public function run()
  {
    Db::table('mag_app_categories')->insert([
      [ 'name' => 'Насосы 1', 'parent_id' => '#', 'weight'=> 1],
      [ 'name' => 'Стабилизаторы 2', 'parent_id' => '#', 'weight'=> 2],
      [ 'name' => 'Генираторы 3', 'parent_id' => 4, 'weight'=> 3],
      [ 'name' => 'Баки расширительные 4', 'parent_id' => '#', 'weight'=> 4],
      [ 'name' => 'Баки водоснабжения 5', 'parent_id' => 4, 'weight'=> 5],
      [ 'name' => 'Баки отопления 6', 'parent_id' => 4, 'weight'=> 6],
      [ 'name' => 'Баки ГВС 7', 'parent_id' => 4, 'weight'=> 7],
      [ 'name' => 'Емкости 8', 'parent_id' => '#', 'weight'=> 8],
      [ 'name' => 'Котлы 9', 'parent_id' => '#', 'weight'=> 9],
      [ 'name' => 'Электрические 10', 'parent_id' => 9, 'weight'=> 10],
      [ 'name' => 'Газовые 11', 'parent_id' => 9, 'weight'=> 11],
      [ 'name' => 'Твердотоплевные 12', 'parent_id' => 9, 'weight'=> 12],
      [ 'name' => 'Колонки 13', 'parent_id' => '#', 'weight'=> 13],
      [ 'name' => 'Запчасти 14', 'parent_id' => '#', 'weight'=> 14],
      [ 'name' => 'Для насосов 15', 'parent_id' => 14, 'weight'=> 15],
      [ 'name' => 'Для котлов 16', 'parent_id' => 14, 'weight'=> 16],
      [ 'name' => 'Комплектующие 17', 'parent_id' => '#', 'weight'=> 17]
    ]);

    Category::setDepth();
  }
}