<?php namespace Mag\App\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use Mag\App\Models\Cart;
use Mag\App\Models\CartProduct;
use Mag\App\Models\Product;
use Mag\App\Models\ProductTarget;
use Auth;

class Devel extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'app:devel';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        //$this->output->writeln('Hello world!');
        dd(intval('50_cat'));
        print $this->myFork2();
    }

    public function myFork2() {

        $pid = pcntl_fork();

        if ($pid != 0) {
            return "200 \n";
        }else{
            sleep(1);
            return "setCar \n";
        }
    }

    public function myFork() {
        //$parentPid = posix_getpid();
        //echo "parentPid ".$parentPid."\n";
        $child1 = pcntl_fork();
        if ($child1 != 0) {
            $child2 = pcntl_fork();
            if ($child2 != 0) {
                echo "Parent \n";
            } else {
                sleep(2);
                echo "Child-worker2 \n";
            }
        } else {
            sleep(1);
            echo "Child-worker1 \n";
        }

    }


    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    public function test2(){

        $prod = Product::find(1);
        dd($prod->delete());
        //$cart = Cart::find(1);//dd($cart);
        //$cart = Cart::count();//dd($cart);
        //$cart = Cart::withTrashed()->find(1)->restore();//dd($cart);

        //$cart->trashed();
        //$cart->delete();
        //dd($cart->trashed());
        //dd($cart);

//        dd($cart->products()->create([
//          'product_id' => 1
//        ]));

        try{
            $cart = Cart::firstOrCreate(['user_id' => 1]);
            $cart->products()->create([
              'product_id' => 1
            ]);
        }catch(\Illuminate\Database\QueryException $e){
            dd($e->getMessage());
            dd($e->getCode());
        }

    }


    public function test(){
        //Auth::attempt(['phone' => '79289006005'], true);
        ///dd(Auth::attempt(['phone' => '79289006005'], true));
        $user = (Auth::check()) ? Auth::user()->phone : null ;
        dd($user);
    }

    public function handle2()
    {
        $typeTovar = [
            'Mag\App\Models\ProductDefault' => 'Mag\App\Models\ProductDefault'
        ];

        $query = DB::table('mag_app_products')
          ->join('mag_app_product_defaults', function ($join) {
              $join->on('mag_app_products.imageable_id', '=', 'mag_app_product_defaults.id')
                   ->whereIn('mag_app_product_defaults.category_id', [1,2,4]);
        })
          ->select('*')->get();

//            $default = Product::where(function($query) {
//                $query->get();
//                  //->orWhere('votes', '>', 100);
//            })
//          ->get();

        //$users = $query->addSelect('id')->get();

        dd($query);


    }

}