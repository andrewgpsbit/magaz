<?php namespace Mag\App\Models;

use Model;

/**
 * CartProduct Model
 */
class CartProduct extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mag_app_cart_products';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'cart_id',
        'product_target_id'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
      //'product' => ['Mag\App\Models\ProductTarget', 'key' => 'id', 'otherKey' => 'product_target_id' ]
    ];
    public $hasMany = [

    ];
    public $belongsTo = [
        'product' => ['Mag\App\Models\ProductTarget', 'key' => 'product_target_id', 'otherKey' => 'id' ]
    ];
    public $belongsToMany = [
        'cart' => ['Mag\App\Models\Cart'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
