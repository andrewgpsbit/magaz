<?php namespace Mag\App\Models;

use Model;
use Mag\App\Controllers\Alias;

/**
 * ProductPump Model
 */
class ProductPump extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'mag_app_product_pumps';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
      'sku',
      'price'
    ];

    /**
     * @var array Fillable fields
     */
    protected $appends= [
      'price_mix'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
        //'category' => ['Mag\App\Models\Category'],
        //'product_default' => ['Mag\App\Models\ProductDefault'],
    ];
    public $hasMany = [];
    public $belongsTo = [
      'category' => ['Mag\App\Models\Category'],
      'brand' => ['Mag\App\Models\Brand'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [
      'target' => ['Mag\App\Models\ProductTarget', 'name' => 'imageable']
    ];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
      'photos' => 'System\Models\File', //'delete' => true
    ];

    public function delete()
    {
        // delete all related photos
        if(!$this->target()->delete()){
            dd('target not delete '.self::class);
        }
        // as suggested by Dirk in comment,
        // it's an uglier alternative, but faster
        // Photo::where("user_id", $this->id)->delete()
        // delete the user
        return parent::delete();
    }

    public function getPriceMixAttribute()
    {
        if(!empty($this->attributes['discount'])){
            return $this->attributes['discount'];
        }else{
            return $this->attributes['price'];
        }
    }

    public function beforeSave()
    {
        if(empty($this->url)){
            $this->url = Alias::generateAlias($this->name);
            $this->url .= '_'. rand(000,999);
        }
    }

    public function afterSave()
    {
        ProductTarget::firstOrCreate(['imageable_type' => self::class, 'imageable_id' => $this->id]);
    }

    public function getCategories(){

        $result = Category::orderBy('weight')->get()->keyBy('id');

        $result->transform(function($item){
            $prefix = ' ';
            for($i = 0; $i < $item['depth']; $i++){
                $prefix .= '&#8212; ';
            }
            return $prefix.$item['name'];
        });

        return $result;
    }

    public function getBrands() {
        $result = Brand::get()->keyBy('id');

        $result->transform(function($item){
            return $item['name'];
        });
        $result[0] = 'Не выбрано';
        return $result;
    }
}
