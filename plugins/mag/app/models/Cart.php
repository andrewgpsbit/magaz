<?php namespace Mag\App\Models;

use Model;

/**
 * Cart Model
 */
class Cart extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mag_app_carts';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id',
        'user_id'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
      //'products' => ['Mag\App\Models\CartProduct', 'key' => 'cart_id',],
      //'products' => ['Mag\App\Models\CartProduct', 'key' => 'cart_id', 'otherkey' => 'id',],
    ];
    public $hasMany = [
      'products' => ['Mag\App\Models\CartProduct', 'softDelete' => true]
    ];

//    public function products()
//    {
//        return $this->hasMany('Mag\App\Models\CartProduct');
//    }
    public $belongsTo = [

    ];
    public $belongsToMany = [
      //'app' => ['Mag\App\Models\CartProduct'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
