<?php namespace Mag\App\Models;

use Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Hash;

/**
 * User Model
 */
class User extends Model implements AuthenticatableContract
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mag_app_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

//    public $appends = [
//        'remember_token'
//    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'phone',
        'email',
        'name',
        'active',
        'remember_token'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

//    public function getRememberTokenAttribute()
//    {
//        return $this->attributes['phone'];
//        //return Hash::make($this->attributes['phone']);
//    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->attributes['id'];
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->attributes['phone'];
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        //dd('#getRememberToken', $this->remember_token);
        return $this->attributes['remember_token'];
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->attributes['remember_token'] = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }



}