<?php namespace Mag\App\Models;

use Model;

/**
 * Tip Model
 */
class Tip extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mag_app_tips';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}