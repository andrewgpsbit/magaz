<?php namespace Mag\App\Models;

use Model;
use Mag\App\Controllers\Alias;

/**
 * Category Model
 */
class Category extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mag_app_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['id_text'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [
      //'imageable_alias' => ['Mag\App\Models\Alias', 'name' => 'imageable']
    ];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];



    public function beforeSave()
    {
        if(empty($this->url)){
            $this->url = Alias::generateAlias($this->name);
        }
    }

    public function getIdTextAttribute()
    {
        return 'id_'.$this->attributes['id'];
    }
}