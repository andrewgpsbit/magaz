<?php namespace Mag\App\Models;

use Model;

/**
 * ProductTarget Model
 */
class ProductTarget extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'mag_app_product_targets';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'imageable_type',
        'imageable_id',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [
      'imageable' => []
    ];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
