<?php namespace Mag\App;

use Backend;
use System\Classes\PluginBase;

/**
 * app Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
          'name'        => 'app',
          'description' => 'No description provided yet...',
          'author'      => 'mag',
          'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('app.devel', 'Mag\App\Console\Devel');
        $this->registerFormWidgets();
    }

    public function registerFormWidgets()
    {
        return [
          'Backend\FormWidgets\Toolbar' => 'toolbar',
          'Backend\FormWidgets\RichEditor' => 'richeditor'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        //return []; // Remove this line to activate

        return [
          'Mag\App\Components\Layout' => 'layout',
          'Mag\App\Components\Product' => 'product',
          'Mag\App\Components\AuthMag' => 'auth_mag',
          'Mag\App\Components\Cart' => 'cart',
          'Mag\App\Components\HomePage' => 'homepage',
        ];

    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
          'mag.app.some_permission' => [
            'tab' => 'app',
            'label' => 'Some permission'
          ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
          'category' => [
            'label'       => 'Категории',
            'url'         => Backend::url('mag/app/category'),
            'icon'        => 'icon-sitemap',
            'permissions' => ['mag.app.*'],
            'order'       => -9,
          ],
          'brand' => [
            'label'       => 'Бренды',
            'url'         => Backend::url('mag/app/brand'),
            'icon'        => 'icon-tags',
            'permissions' => ['mag.app.*'],
            'order'       => -8,
          ],
          'product' => [
            'label'       => 'Продукт',
            'url'         => Backend::url('mag/app/product'),
            'icon'        => 'icon-tag',
            'permissions' => ['mag.app.*'],
            'order'       => -7,
          ],
          'productpump' => [
            'label'       => 'Продукт насос',
            'url'         => Backend::url('mag/app/productpump'),
            'icon'        => 'icon-tag',
            'permissions' => ['mag.app.*'],
            'order'       => -6,
          ],
          'tip' => [
            'label'       => 'Загрузка товара',
            'url'         => Backend::url('mag/app/tip'),
            'icon'        => 'icon-tag',
            'permissions' => ['mag.app.*'],
            'order'       => -5,
          ],
          'contact' => [
            'label'       => 'Контакты',
            'url'         => Backend::url('mag/app/contact/update/1'),
            'icon'        => 'icon-tag',
            'permissions' => ['mag.app.*'],
            'order'       => -4,
          ]
        ];
    }

}