<?php namespace Mag\App\Components;

use Cms\Classes\ComponentBase;
use Mag\App\Models\Product as MProduct;
use Mag\App\Models\ProductPump as MProductPump;
use Mag\App\Models\Category as MCategory;

class Product extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Product Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        $product = $this->param('product');
        //dd($product);
        //preg_match('/_[0-9]+$/', $product, $matches);
        //$productId = preg_replace('/_/', '', current($matches));

        $this->page['entity'] = $this->getProduct($product);
        return [];
    }

    public function getProduct($productId){
        $oProduct = MProduct::where('url', $productId)->first();
        $oProductPump = MProductPump::where('url', $productId)->first();
        switch(true){
            case $oProduct:
                $product = $oProduct;
            break;
            case $oProductPump:
                $product = $oProductPump;
            break;

        }
        //dd($product);
        return $this->prepareProduct($product);
    }

    public function prepareProduct($oProduct){
        //dd($oProduct);
        $photos = [];
        foreach($oProduct->photos as $photo){
            $photos[] = $photo->getThumb(750, 557, ['mode' => 'crop']);
        }
//dd($oProduct->imageable);
        return [
            'product' => $oProduct->toArray(),
            'prdoduct_type' => $oProduct->toArray(),
            'photos' => $photos,
            'category' => MCategory::find($oProduct->category_id)->toArray()
        ];
    }


}