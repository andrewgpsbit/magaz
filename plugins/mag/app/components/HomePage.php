<?php namespace Mag\App\Components;

use Cms\Classes\ComponentBase;
use Mag\App\Models\Product;
use Mag\App\Models\ProductPump;
use Mag\App\Models\Category;
use Mag\App\Models\Brand;
use Mag\App\Models\Tip;
use Response;
use DB;


class HomePage extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'HomePage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        $this->myRender();

        return [];
    }

    public function myRender(){
        $this->addCss('/themes/corlate/assets/pages/homePage/home.css');
        $this->addCss('/plugins/mag/app/assets/bower_components/jstree/dist/themes/default/style.css');
        $this->addCss('/plugins/mag/app/assets/js/slick/slick.css');
        $this->addCss('/plugins/mag/app/assets/js/slick/slick-theme.css');

        $this->addJs('/themes/corlate/assets/pages/homePage/home.js');
        $this->addJs('/plugins/mag/app/assets/bower_components/jstree/dist/jstree.min.js');
        $this->addJs('/plugins/mag/app/assets/js/slick/slick.min.js');

        $this->getProducts();
        $this->getBrands();
        $this->getTips();
    }

    public function onGetProductsByCategory(){

        $current = post('id');
        $childs = Category::where('parent_id', $current)->get()->pluck('id')->toArray();
        $categories = array_merge([$current], $childs);

        $oProducts = Product::whereIn('category_id', $categories)->with('category')->get();
        $oProductPumps = ProductPump::whereIn('category_id', $categories)->with('category')->get();

        $aProducts = $this->prepareProducts($oProducts);
        $aProductPumps = $this->prepareProducts($oProductPumps);
        $prepare = array_merge($aProducts, $aProductPumps);

        $oCart = new Cart();
        $aCart = $oCart->onGetCart();
        $aCartIds = [];
        if($aCart['code'] == 200){
            $aCart['products']->transform(function($item, $key) use(&$aCartIds) {
                $return = (!empty($item['idTarget'])) ? $item['idTarget'] : '';
                return $return;
            });
            $aCartIds = $aCart['products']->toArray();
        }
        //dd($aCartIds);

//        $aProducts->products->transform(function($item, $key){
//            $return['idTarget'] = (!empty($item->product->imageable->target->id)) ? $item->product->imageable->target->id : '';
//            return $return;
//        });

        return $this->renderPartial('homepage/products',
          [
            'products' => $prepare,
            'aCartIds' => $aCartIds,

          ]);
    }

//    public function onGetProductsByCategory(){
//        $current = [post('current')];
//        $childs = (!empty(post('childs')))? post('childs') : [];
//        $categories = array_merge($current, $childs);
//
//        $oProducts = Product::get();
//        $filtered = $oProducts->reject(function ($item) use ($categories){
//            return empty($item->imageable_type) || !in_array($item->imageable->category->id, $categories );
//        });
//
//        $prepare = $this->prepareProducts($filtered);
//        //dd($prepare);
//        return $this->renderPartial('homepage/products',
//          [
//            'products' => $prepare
//          ]);
//    }

    public function getProducts(){
        $oProducts = Product::orderBy('id', 'asc')->where('category_id',1)->get();
        $oProductPumps = ProductPump::orderBy('id', 'asc')->get();
        $aProduct = $this->prepareProducts($oProducts);
        $aProductPumps = $this->prepareProducts($oProductPumps);
        $this->page['products'] = array_merge($aProduct, $aProductPumps);
    }

    public function prepareProducts($oProducts){

        $oProducts->transform(function ($item, $key){
            $photos = [];
            foreach($item->photos as $photo){
               $photos[] = $photo->getThumb(140, 140, ['mode' => 'crop']);
            }
            if(empty($photos)){
                $photos = ['/themes/corlate/assets/images/no_photo_small.png'];
            }
            //dd($item->target->id);
            return [
              'id' => $item->target->id,
              'sku' => $item->sku,
              'price' => $item->price,
              'price_mix' => $item->price_mix,
              'discount' => $item->discount,
              'category' => [
                  'id' => $item->category->id,
                  'name' => $item->category->name,
                  'alias' => $item->category->url
              ],
              'name' => $item->name,
              'alias' => $item-> url,
              'desc' => $item->desc,
              'photos' => $photos,
              'p1' => $item->p1,
              'hmax' => $item->hmax,
              'qmax' => $item->qmax
            ];
        });

        //dd($oProducts->toArray());
        return $oProducts->toArray();
    }

    public function onGetCategories(){
        $categories = Category::orderBy('weight')->get()->transform(function ($item, $key) {
            $parent = ($item->parent_id) ? $item->parent_id : '#' ;
            $opened = ($item->id == 1) ? true : false;
            return [ "id" => $item->id, "parent" => $parent, "text" => $item->name, "state" => ["opened" => $opened],
              "href"=>'/backend/mag/app/category/update/'.$item->id ];
        })->toArray();
        $return = [
          "id"=>1,
          "text"=>"Root node",
          "state" => ["opened" => true ],
          "children" => $categories
        ];

        return ['categories' => $categories];
    }

    public function getBrands() {

        $oBrands = Brand::get();

        $oBrands->transform(function ($item, $key) {
            return [
              'id' => $item->id,
              'name' => $item->name,
              'photo' => ($item->photo) ? $item->photo->getThumb(166, 88, ['mode' => 'crop']) : ''
            ];
        });

        return $this->page['brands'] = $oBrands->toArray();
    }

    public function getTips() {

        $oTips = Tip::get();

        $oTips->transform(function ($item, $key) {
            return [
              'id' => $item->id,
              'title' => $item->title,
              'text' => $item->text,
            ];
        });

        return $this->page['tips'] = $oTips->toArray();
    }

}