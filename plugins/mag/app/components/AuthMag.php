<?php namespace Mag\App\Components;

use Cms\Classes\ComponentBase;

use Mag\App\Models\User;
use Validator;
use Auth;

class AuthMag extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Auth Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        $this->myRender();
        return [];
    }

    public function myRender() {
        //Auth::logout();
        //dd(Auth::check());
        //Auth::attempt(['phone' => '79289006005']);
        return $this->page['auth'] = (Auth::user()) ? Auth::user()->phone : null ;
    }

    public function onLogin() {
        $phone = str_replace([' ','+','(',')','-'], '', post('phone'));
        $user = User::firstOrCreate(['phone' => $phone]);
        if($user){
            if(Auth::attempt(['phone' => $user->phone], true)){
                return redirect()->intended('/');
            }
        }
    }

    public function onLogout() {
        Auth::logout();
        sleep(0.1);
        return redirect()->intended('/');

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
          //'name' => 'required|max:255',
          //'email' => 'required|email|max:255|unique:users',
          'phone' => 'required|phone|max:5|unique:users',
          //'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
          'phone' => $data['name'],
          //'name' => $data['name'],
          //'email' => $data['email'],
          //'password' => bcrypt($data['password']),
        ]);
    }
}
