<?php namespace Mag\App\Components;

use Cms\Classes\ComponentBase;
use Doctrine\DBAL\Driver\PDOException;
use Mag\App\Models\Cart as MCart;
use Mag\App\Models\CartProduct;
use Illuminate\Database\QueryException ;
use Auth;

class Cart extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Cart Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        $this->myRender();
        return [];
    }

    public function myRender() {
        return 'hello';
    }

    public function onGetCart() {
        if(!Auth::check()){
            return ['code'=>406, 'message'=>'Auth not found'];
        }
        $userId = Auth::user()->id;
        $cart = MCart::where('user_id', $userId)->first();
        if($cart){
            $products = $this->getCartProducts($cart);
        }else{
            return ['code'=>406, 'message'=>'Cart not found'];
        }

        return ['code'=>200, 'products'=>$products];
    }

    public function onSetCart() {
        if(!Auth::check() || !post('id') ){
            return ['code'=>503, 'message'=>'Auth not found or id product'];
        }
        $userId = Auth::user()->id;
        $productId = post('id');

        try{
            $cart = MCart::firstOrCreate(['user_id' => $userId]);
            $cart->products()->create([
              'product_target_id' => $productId
            ]);
        }catch(QueryException $e){

            return ['code'=>$e->getCode(),'message'=>$e->getMessage()];
        }

        $products = $this->getCartProducts($cart);

        return ['code'=>200, 'message'=>'Ok', 'products' => $products];
    }

    public function onDeleteCart() {
        if(!Auth::check() || !post('id') ){
            return ['code'=>406, 'message'=>'Auth not found or id product'];
        }
        $userId = Auth::user()->id;
        $productId = post('id');

        try{
            $cart = MCart::where('user_id', $userId)->first();
            $cart->products()
              ->where('product_target_id', $productId)
              ->forceDelete();
        }catch(QueryException $e){

            return ['code'=>$e->getCode(),'message'=>$e->getMessage()];
        }

        $products = $this->getCartProducts($cart);

        return ['code'=>200, 'message'=>'Ok', 'products' => $products];
    }

    public function getCartProducts($cart) {

        $cart->products->transform(function($item, $key){
            $return = $item->product->imageable->toArray();
            //dd($item->product->imageable->photos[0]);
            $return['idTarget'] = (!empty($item->product->imageable->target->id)) ? $item->product->imageable->target->id : '';
            $return['photo'] = (!empty($item->product->imageable->photos[0])) ? $item->product->imageable->photos[0]->getThumb(60, 60, ['mode' => 'crop']) : '/themes/corlate/assets/images/no_photo_small.png';
            return $return;
        });
        return $cart->products;
    }
}
