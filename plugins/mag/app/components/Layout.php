<?php namespace Mag\App\Components;

use Cms\Classes\ComponentBase;
use Mag\App\Models\Category;
use Mag\App\Models\Contact;

use Auth;

class Layout extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Layout Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        $this->addCss('/plugins/mag/app/assets/js/fancybox/source/jquery.fancybox.css');

        //$this->addJs('/plugins/mag/app/assets/js/fancybox/lib/jquery-1.9.0.min.js');
        $this->addJs('/plugins/mag/app/assets/js/fancybox/source/jquery.fancybox.pack.js');
        $this->addJs('/plugins/mag/app/assets/js/maskedinput.min.js');

        $this->myRender();
        $this->getContact();


        return [];
    }

    public function myRender() {

        $query = Category::orderBy('weight');
        $depthhMax = $query->max('depth');
        $cateories = $query->get()->keyBy('id')->toArray();
        $tree = [];

        function array_find_deep($array, $search, $keys = array())
        {
            foreach($array as $key => $value) {
                if (is_array($value)) {
                    $sub = array_find_deep($value, $search, array_merge($keys, array($key)));
                    if (count($sub)) {
                        return $sub;
                    }
                } elseif ($value === $search) {
                    return array_merge($keys, array($key));
                }
            }

            return array();
        }

        for( $i=0; $i <= $depthhMax; $i++ ){
            //var_dump($i);
            foreach($cateories as $catId => $cateory ){

                if($i == $cateory['depth']){

                    foreach($cateories as $catId2 => $cateory2 ){

                        if($catId == $cateory2['parent_id']){

                            $find = array_find_deep($tree, 'id_'.$catId);
                            if($find) {

                                switch(count($find)) {
                                    case 3:
                                        $tree[$find[0]][$find[1]]['childs'][$catId2] = $cateory2;
                                    break;
                                    case 4:
                                        $tree[$find[0]][$find[1]][$find[2]]['childs'][$catId2] = $cateory2;
                                    break;
                                    case 5:
                                        $tree[$find[0]][$find[1]][$find[2]][$find[3]]['childs'][$catId2] = $cateory2;
                                    break;
                                    case 6:
                                        $tree[$find[0]][$find[1]][$find[2]][$find[3]][$find[4]]['childs'][$catId2] = $cateory2;
                                    break;
                                }
                            }else{
                                $tree[$catId]['id'] = $cateory['id'];
                                $tree[$catId]['name'] = $cateory['name'];
                                $tree[$catId]['childs'][$catId2] = $cateory2;
                            }
                        }elseif($cateory['depth'] == 0){
                            $tree[$catId]['id'] = $cateory['id'];
                            $tree[$catId]['name'] = $cateory['name'];
                        }
                    }
                }
            }
        }
        $this->page['main_menu'] = $tree;
    }

    public function getContact() {
        $contact = Contact::first()->toArray();
        $this->page['phone'] = (empty($contact['phone'])) ? '' : $contact['phone'] ;
        $this->page['email'] = (empty($contact['email'])) ? '' : $contact['email'] ;
        return $this->alias;
    }

    public function onSendEmail() {
        dd(post());
        return ;
    }

}