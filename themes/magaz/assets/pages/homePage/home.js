$(function(){
    var products = $('#products');

    function initTree(data) {
        $('#tree').jstree({
            core: {
                data: data,
                check_callback: true
            }
        });

        $('#tree').on('changed.jstree', function (e, data) {
            console.log('changed', data);
            console.log('changed products', $('#products'));
            $('#products').attr('class', 'load');
            $.request('homepage::onGetProductsByCategory', {
                data: {current: data.node.id, childs: data.node.children_d},
                complete: function (data) {
                    //$('#products').removeClass('load');
                    this.complete(data);
                },
                success: function(data) {
                    data.result = (data.result == undefined) ? '' : data.result;
                    console.log('success', data.result);
                    $('.products').html(data.result);
                    products.removeClass('load');
                    this.success(data);
                }
            });

        });
    }


    $.request('homepage::onGetCategories', {
        complete: function (data) {
            $.oc.stripeLoadIndicator.hide();
            this.complete(data);
        },
        success: function(data) {
            //console.log('success', data.categories);
            initTree(data.categories);

            this.success(data);
        }
    });
});