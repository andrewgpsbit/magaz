jQuery(function($) {'use strict',

	//#main-slider
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 8000
		});
	});


	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Initiat WOW JS
	new WOW().init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),

			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
		});
	});

	
	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});


	$('#online-consultant-href').fancybox({
		maxWidth: 600,
		helpers: {
			overlay: {
				locked: false
			}
		}
	});

	$('#form-login-href').fancybox({
		maxWidth: 600,
		helpers: {
			overlay: {
				locked: false
			}
		}
	});

	$('#form-logout-href').fancybox({
		maxWidth: 600,
		helpers: {
			overlay: {
				locked: false
			}
		}
	});

	$("#phone, #phone-login").mask("+7(999) 999-99-99");

	function validate(that){
		var aInput = that.serializeArray();
		var flag = true;
		for( var key in aInput ){
			console.log(aInput[key])
			if(aInput[key].value == ''){
				that.find('[name="'+aInput[key].name+'"]').closest('.form-row').addClass('error');
				flag = false;
			}
		}
		return flag;
	}

	$('#online-consultant').submit(function(e){
		e.preventDefault();
		that = $(this);

		if(validate(that)){
			var aInput = that.serializeArray();
			//alert('ща отправлю')
			$.request('layout::onSendEmail', {
				data: aInput,
				complete: function (data) {
					$.oc.stripeLoadIndicator.hide();
					this.complete(data);
				},
				success: function(data) {

					this.success(data);
				}
			});
		}
	});

	$('#form-login').submit(function(e){
		e.preventDefault();
		that = $(this);

		if(validate(that)){
			var aInput = that.serializeArray();
			//alert('ща отправлю')
			$.request('auth_mag::onLogin', {
				data: aInput,
				complete: function (data) {
					$.oc.stripeLoadIndicator.hide();
					this.complete(data);
				},
				success: function(data) {

					this.success(data);
				}
			});
		}
	});

	$('#form-logout').submit(function(e){
		e.preventDefault();
		$.request('auth_mag::onLogout', {
			complete: function (data) {
				$.oc.stripeLoadIndicator.hide();
				this.complete(data);
			},
			success: function(data) {
				this.success(data);
			}
		});
	});



	$('.forms input, .forms textarea').keyup(function(e){
		$(this).closest('.form-row').removeClass('error');
	});

});