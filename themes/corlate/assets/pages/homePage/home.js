jQuery(function($){
    var products = $('#products');


    $('.navbar-nav li a').click(function(e){
        e.preventDefault();
        var id = $(this).closest('li').data('id');
        //console.log(id)

        products.addClass('load');
        $.request('homepage::onGetProductsByCategory', {
            data: {id: id},
            complete: function (data) {
                //$('#products').removeClass('load');
                this.complete(data);
            },
            success: function(data) {
                data.result = (data.result == undefined) ? '' : data.result;
                //console.log('success', data.result);
                $('.container > .products').html(data.result);

                products.removeClass('load');
                this.success(data);
            }
        });

    });

    function initTree(data) {
        $('#tree').jstree({
            core: {
                data: data,
                check_callback: true
            }
        });

        $('#tree').on('changed.jstree', function (e, data) {
            console.log('changed', data);
            console.log('changed products', $('#products'));
            products.addClass('load');
            $.request('homepage::onGetProductsByCategory', {
                data: {current: data.node.id, childs: data.node.children_d},
                complete: function (data) {
                    //$('#products').removeClass('load');
                    this.complete(data);
                },
                success: function(data) {
                    data.result = (data.result == undefined) ? '' : data.result;
                    console.log('success', data.result);
                    $('.products').html(data.result);
                    products.removeClass('load');
                    this.success(data);
                }
            });

        });
    }


    $.request('homepage::onGetCategories', {
        complete: function (data) {
            $.oc.stripeLoadIndicator.hide();
            this.complete(data);
        },
        success: function(data) {
            //console.log('success', data.categories);
            initTree(data.categories);
            products.removeClass('load');
            this.success(data);
        }
    });


    $(".partners ul").slick({
        //dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4
    });

    $(".partners li").click(function(e){
        e.preventDefault();
    });


    $('#tips a').fancybox({
        maxWidth: 600,
        helpers: {
            overlay: {
                locked: false
            }
        }

    });

    $('.products').on('click', '[data-active="added"]', function(e){
        e.preventDefault();
        var id = $(this).data('product-id');
        console.log('id', id);
        $.request('cart::onSetCart', {
            data: {id: id},
            complete: function (data) {
                //$.oc.stripeLoadIndicator.hide();
                this.complete(data);
            },
            success: function(data) {
                //console.log('success', data);
                if(data.code == 200){
                    renderCart(data.products);
                }
                if(data.code == 503){ // auth
                    $('#form-login-href').click();
                }

                this.success(data);
            }
        });
    });

    $('#cart-href').fancybox({
        //padding: 0,
        topRatio: 0,
        wrapCSS: 'cart',
        leftRatio: 1.025,
        //openMethod: 'dropIn',
        openSpeed: 250,
       // closeMethod: 'dropOut',
        closeSpeed: 150,
        //closeBtn: false,
        //helpers: {
        //    overlay: {
        //        locked: false
        //    }
        //}
    });

    var btnCart = $('<a id="cart-href" class="fancybox">').attr('href', '#cart').text('Ваша корзина');
    var cart = $('#cart .content');
    function renderCart(prods){
        if(prods.length == 0){
            $.fancybox.close();
            $('#btn-cart span').text('Корзина пуста');
        }else{
            $('#btn-cart span').html(btnCart);
        }
        products = $('<div class="products">');
        for(var key in prods ){
            item = prods[key];
            //console.log('prod', item);
            photo = $('<img class="photo">').attr('src', item.photo);
            title = $('<a class="title">').attr('href', '/product/'+item.url).text(item.name);
            price = $('<span class="price">').attr('data-price', item.price_mix).text(item.price_mix+' руб.');
            del = $('<span class="delete">');
            itemBox = $('<div class="item">').attr('data-id', item.idTarget).append(photo).append(title).append(price).append(del);
            products.append(itemBox);

            $('[data-product-id="'+item.idTarget+'"]').attr('data-active', 'not');
        }

        cart.html(products);
        calculate(prods);
    }

    function calculate(prods){
        sum = 0;
        for(var key in prods ){
            item = prods[key];
            price = parseInt(item.price_mix);
            sum += price;
        }
        $('#cart .calculate').text('Всего: '+sum+' руб.');
    }

    $.request('cart::onGetCart', {
        complete: function (data) {
            $.oc.stripeLoadIndicator.hide();
            this.complete(data);
        },
        success: function(data) {
            //console.log('success', data);
            if(data.code == 200){
                renderCart(data.products);
            }

            this.success(data);
        }
    });


    $("#cart").on("click", ".item .delete", function(e){
        e.preventDefault();
        var id = $(this).closest('.item').data('id');
        console.log('id', id);
        $.request('cart::onDeleteCart', {
            data: {id: id},
            complete: function (data) {
                //$.oc.stripeLoadIndicator.hide();
                this.complete(data);
            },
            success: function(data) {
                //console.log('success', data);
                if(data.code == 200){
                    $('.btn-cart-product').attr('data-active', 'added');
                    renderCart(data.products);
                }

                this.success(data);
            }
        });
    });


});